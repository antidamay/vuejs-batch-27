export const AboutComponent = {
    data(){
        return {
            categories: [
                {
                    id: 1,
                    title: 'Alat Tulis',
                    description : 'Alat tulis adalah alat yang digunakan untuk menulis, diantaranya pulpen, pensil, penghapus, dll.'
                },
                {
                    id: 2,
                    title: 'Alat Mandi',
                    description : 'Alat tulis adalah alat yang digunakan untuk mandi, diantaranya gayung, sikat gigi, handuk, dll.'

                },
                {
                    id: 3,
                    title: 'Alat Masak',
                    description : 'Alat tulis adalah alat yang digunakan untuk memasak, diantaranya panci, kompor, katel, dll.'

                },
                
            ]
        }
    },
    computed: {
        about() {
            return this.categories.filter((about)=>{
                return about.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template: `<div >
            <h1>About :  {{ about.title }}</h1>
            <ul>
                <li v-for="(num, value) of category">
                    {{ num +' : '+ value }} <br>
                </li>
            </ul>
        </div>`,
   
}