export const HomeComponent = {
    data(){
        return {
            Categories: [
                {
                    id: 1,
                    title: 'Alat Tulis',
                    description : 'Alat tulis adalah alat yang digunakan untuk menulis, diantaranya pulpen, pensil, penghapus, dll.',
                    example : 'Pensil'
                },
                {
                    id: 2,
                    title: 'Alat Mandi',
                    description : 'Alat tulis adalah alat yang digunakan untuk mandi, diantaranya gayung, sikat gigi, handuk, dll.',
                    example : 'Gayung'
                },
                {
                    id: 3,
                    title: 'Alat Masak',
                    description : 'Alat tulis adalah alat yang digunakan untuk memasak, diantaranya panci, kompor, katel, dll.',
                    example : 'Panci'
                },
            ]
        }
    },
    computed: {
        home() {
            return this.About.filter((info)=>{
                return home.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template: `<div >
            <h1>Home :  {{ home.title }}</h1>
            <ul>
                <li v-for="(num, value) of info">
                    {{ num +' : '+ value }} <br>
                </li>
            </ul>
        </div>`, 
}