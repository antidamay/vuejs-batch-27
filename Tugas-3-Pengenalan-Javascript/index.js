var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
//saya senang belajar JAVASCRIPT

var ketiga = kedua.toUpperCase();

//console.log(pertama[0])+console.;og(pertama[1])+console.log(pertama[2])+console.log(pertama[3])

console.log(pertama[0]+pertama[1]+pertama[2]+pertama[3],
   pertama[12]+pertama[13]+pertama[14]+pertama[15]+pertama[16]+pertama[17],
   kedua[0]+kedua[1]+kedua[2]+kedua[3]+kedua[4]+kedua[5]+kedua[6],
   ketiga[8]+ketiga[9]+ketiga[10]+ketiga[11]+ketiga[12]+ketiga[13]+ketiga[14]+ketiga[15]+ketiga[16]+ketiga[17])

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
//*catatan :
//1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
//2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6)

var number1 = Number(kataPertama)
var number2 = Number(kataKedua)
var number3 = Number(kataKetiga)
var number4 = Number(kataKeempat)
console.log((number1-number3+number4)*number2)

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14) // do your own! 
var kataKetiga = kalimat.substring(15, 18) // do your own! 
var kataKeempat = kalimat.substring(19, 24) // do your own! 
var kataKelima = kalimat.substring(25, 31) // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//Kata Pertama: wah
//Kata Kedua: javascript
//Kata Ketiga: itu
//Kata Keempat: keren
//Kata Kelima: sekali