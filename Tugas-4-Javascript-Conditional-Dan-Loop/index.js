var nilai = 85;
if (nilai >= 85) {
    console.log("A");
} else if (nilai >= 75 && nilai < 85) {
    console.log("B");
} else if (nilai >= 65 && nilai < 75) {
    console.log("C");
} else if (nilai >= 55 && nilai < 65) {
    console.log("D");
} else if (nilai < 55) {
    console.log("E");
}

//SOAL 2
var tanggal = 3;
var bulan = 1;
var tahun = 2000;

switch (bulan){
    case 1 :
        console.log(tanggal + " Januari " + tahun);
        break;
    case 2 :
        console.log(tanggal + " Februari " + tahun);
        break;
    case 3 :
        console.log(tanggal + " Maret " + tahun);
        break;
    case 4 :
        console.log(tanggal + " April " + tahun);
        break;
    case 5 :
        console.log(tanggal + " Mei " + tahun);
        break;
    case 6 :
        console.log(tanggal + " Juni " + tahun);
        break;
    case 7 :
        console.log(tanggal + " Juli " + tahun);
        break;
    case 8 :
        console.log(tanggal + " Agustus " + tahun);
        break;
    case 9 :
        console.log(tanggal + " September " + tahun)
        break;
    case 10 :
        console.log(tanggal + " Oktober " + tahun);
        break;
    case 11 :
        console.log(tanggal + " November " + tahun);
        break;
    case 12 :
        console.log(tanggal + " Desember " );
        break;
    default :
        console.log("Eror");
}

//SOAL 3
//Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n.
//Looping boleh menggunakan syntax apa pun (while, for, do while).

var n = 3;
var p = '';
for (var i = 0; i < n;i++){
    for (var j =0; j <= i; j++){
        p += '#';
    }
    p +='\n';
}
console.log(p);

var s = 7;
var t = '';
for (var i = 0; i < s;i++){
    for (var j =0; j <= i; j++){
        t += '#';
    }
    t +='\n';
}
console.log(t);

//SOAL 4
var m = 10;
var sd = '';
for (var i = 1; i <= m; i+=3){
    console.log(i + " - I love programming");
    var j =i+1;
    var l = j+1;
    sd += '=';
    while (j<l && j<=m){
        console.log(j+ " - I love Javascript");
        var k = j+1;
        var o = k+1;
        sd += '=';
        while ( k<o && k<=m){
            console.log(k+" - I love VueJs");
            sd += '=';
            k+= 3;
        }
    j+=3;
    console.log(sd);
    }
}