//SOAL 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
console.log(daftarHewan);
for (var i = 0; i < daftarHewan.length; i++ ){
    console.log(daftarHewan[i]);
}

//SOAL 2
function introduce (data){
    var name = data.name;
    var age = data.age;
    var address = data.address;
    var hobby = data.hobby;
    return ("Nama saya " + name +"," + " umur saya " + age + "," + " alamat saya di " + address + "," + " dan saya punya hobby yaitu " + hobby + ".");
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data);
console.log(perkenalan);

//SOAL 3

function checkTeks(n){
    return ['a', 'A', 'e', 'E',  'i', 'I', 'o','O', 'u', 'U'].indexOf(n) !== -1;}
    
    function hitung_huruf_vokal(str){
     var strArray = str.split("");
       var jumlahVokal = 0;
        for (var i = 0; i < strArray.length; i++){
           
            if (checkTeks(strArray[i]) === true){
                jumlahVokal++;
            }
        }
        return jumlahVokal;
    }
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2)


//SOAL 4
function hitung(n) {
    var a = (2*n)
    var b = a-2
    return b
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8

