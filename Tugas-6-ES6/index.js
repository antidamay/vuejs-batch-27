//SOAL 1
luas = (a, b) =>{
    let panjang = a;
    let lebar = b;
    return luas = panjang*lebar;
}
keliling = (c, d) => {
    let panjang = c;
    let lebar = d;
    return keliling = 2*panjang + 2*lebar;
}
console.log(luas(10, 8));
console.log(keliling(10, 8));

//SOAL 2
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName()

const NameString = 'Astri Damayanti'
// const will = `${newFunction}`
const will = {NameString}
console.log(will)

//SOAL 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
  //Driver code
  const{firstName, lastName, address, hobby} = newObject
  console.log(firstName, lastName, address, hobby)

//SOAL 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)
var combinedArray = [...west, ...east]
console.log(combinedArray) // ["Will", "Chris", "Sam", "Holly", "Gill", "Brian", "Noel", "Maggie"]

//SOAL 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
const str = `${before}`
console.log(str);